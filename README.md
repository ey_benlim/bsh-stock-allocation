# SDR03 - Stock Allocation Report

### Links ###
* Link to Repository: https://bitbucket.org/ey_benlim/bsh-stock-allocation/src/bd64292ab51ce98bf9c5ec05444c2afa81419f46/?at=release%2Fbase-build
* Link to Functional Specification: 

### Design Decisions ###
*	Fiori Element is used for build
*	Custom fields doesn't support formatting and hence need to be introduced as the custom columns

### Change History ###
|Date      |Reference#|Author     |Description          |
|----------|----------|-----------|---------------------|
|10/11/2021|SDR03	   |Kevin Pang |Base build           |