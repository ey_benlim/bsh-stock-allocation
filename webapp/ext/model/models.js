sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device",
	// "stockallocationfe/ext/model/constant"
], function (JSONModel, Device) {
	"use strict";

	return {

// 		createDeviceModel: function () {
// 			var oModel = new JSONModel(Device);
// 			oModel.setDefaultBindingMode("OneWay");
// 			return oModel;
// 		},
// 		/**
// 		 * Convenience method for creating the message model to store the odata log
// 		 * @public
// 		 * @returns {sap.ui.model.json.JSONModel} the message model for odata log
// 		 */
// 		createMessageModel: function () {
// 			var oModel = new JSONModel({
// 				messages: []
// 			});
// 			return oModel;
// 		},
// 		/**
// 		 * Convenience method for creating the model for worklist
// 		 * @public
// 		 * @returns {sap.ui.model.json.JSONModel} the worklist model
// 		 */
		createWorklistModel: function () {
			var oModel = new JSONModel({
				currentUser: ""
			});
			return oModel;
		}
// 		/**
// 		 * Convenience method for creating the model for constants
// 		 * @public
// 		 * @returns {sap.ui.model.json.JSONModel} the constants model
// 		 */
// 		createConstantModel: function () {
// 			var oModel = new JSONModel(constant);
// 			oModel.setDefaultBindingMode("OneWay");
// 			return oModel;
// 		}

	};
});