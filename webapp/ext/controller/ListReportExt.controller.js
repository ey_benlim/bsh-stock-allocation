 sap.ui.define(["ZSD_STOCK_ALLOCATION/ext/model/models",
 	"ZSD_STOCK_ALLOCATION/ext/controller/BaseController",
 	"sap/m/MessageBox",
 	"sap/m/MessageToast",
 	"sap/m/BusyDialog",
 	"sap/ui/core/Item",
 	"sap/ui/model/Sorter",
 	"sap/ui/model/Filter",
 	"sap/ui/model/FilterOperator",
 	"sap/ui/table/RowSettings",
 	"sap/m/MessagePopover",
 	"sap/m/MessageItem"
 ], function (models, BaseController, MessageBox, MessageToast, BusyDialog, Item, Sorter, Filter, FilterOperator, RowSettings,
 	MessagePopover, MessageItem) {
 	return {

 		onInit: function () {
 			Object.assign(this, BaseController);
 			// this._oWorklistModel = models.createWorklistModel();
 			// this.setModel(this._oWorklistModel, "worklist");
 			this._oSmartTable = this.byId("listReport");
 			this._oSmartTable.getTable().getPlugins()[0].setSelectionMode("MultiToggle");
 			this._oFilterBar = this._oSmartFilterBar = this.byId("listReportFilter");
 			this._oSmartFilterBar.attachEvent("initialized", this.onFilterInitialized.bind(this));
 			this._oSmartFilterBar.attachEvent("search", this.onSearch.bind(this));
 			this.byId("showMessagesButton").bindProperty("visible", "{path:'{=${message>/}.length > 0}");
 		},
 		onSearch: function (oEvent) {
 			var oFilterBar = oEvent.getSource();
 			var aFilterCount = oFilterBar.getAllFiltersWithValues();
 			if (aFilterCount.length === 0) {
 				MessageBox.warning(this.getResourceText("noFilterMsg"), {
 					title: "Warning"
 				});
 			}
 		},
 		onFilterInitialized: function (e) {
 			this._oModel = this.getModel();
 			this._oSmartTable.getTable().setThreshold(1000);
 		},
 		handleQuantityChange: function (oEvent) {
 			this.setAppBusy(true);

 			var oBindingContext = oEvent.getSource().getBindingContext(),
 				sPath = oBindingContext.getPath(),
 				shippingPoint = oBindingContext.getObject().ShippingPoint,
 				partNumber = oBindingContext.getObject().PartNumber,
 				aKeys = oEvent.getSource().getParent().getTable().getBinding()["aKeys"],
 				oFloat,
 				oAmount = 0;

 			if (isNaN(parseFloat(oEvent.getSource().getValue())) && oEvent.getSource().getValue() !== "") {
 				// is not float    
 				oFloat = false;
 			}
 			if (oFloat !== false) {

 				//first loop to get the total of accumulated qty based on part number
 				for (var t = 0; t < aKeys.length; t++) {
 					if (partNumber === this.getModel().getProperty("/" + aKeys[t] + "/PartNumber") && shippingPoint === this.getModel().getProperty(
 							"/" + aKeys[t] + "/ShippingPoint")) {
 						if (!isNaN(parseFloat(this.getModel().getProperty("/" + aKeys[t] + "/AllocatedQty")))) {
 							// is float    
 							// means when empty, means deleted value from table thus have to add back the value
 							// if (sPath === "/" + aKeys[t]) {
 							// 	if (oEvent.getSource().getValue() !== "") {
 							// 		oAmount = +oAmount + +this.getModel().getProperty("/" + aKeys[t] + "/AllocatedQty");

 							// 	}
 							// 	oAmount = +oAmount + +this.getModel().getProperty("/" + aKeys[t] + "/DeliveryQty");
 							// } else {
 							// 	oAmount = +oAmount + +this.getModel().getProperty("/" + aKeys[t] + "/AllocatedQty");
 							// 	oAmount = +oAmount + +this.getModel().getProperty("/" + aKeys[t] + "/DeliveryQty");
 							// }
 							debugger;
 							oAmount = +oAmount + +this.getModel().getProperty("/" + aKeys[t] + "/AllocatedQty");
 						}
 						oAmount = +oAmount + +this.getModel().getProperty("/" + aKeys[t] + "/DeliveryQty");
 					}
 				}
 				for (t = 0; t < aKeys.length; t++) {
 					if (partNumber === this.getModel().getProperty("/" + aKeys[t] + "/PartNumber") && shippingPoint === this.getModel().getProperty(
 							"/" + aKeys[t] + "/ShippingPoint")) {

 						if (shippingPoint === '1101') {
 							this.getModel().setProperty("/" + aKeys[t] + "/BalanceQty1200", this.getModel().getProperty("/" + aKeys[t] +
 								"/UnrestrictedQty1200") - oAmount);
 						}
 						if (shippingPoint === '1102') {
 							this.getModel().setProperty("/" + aKeys[t] + "/BalanceQty11r0", this.getModel().getProperty("/" + aKeys[t] +
 								"/UnrestrictedQty11r0") - oAmount);
 						}
 					}
 				}
 				// var iSelectedRow = oEvent.getSource().getParent().getIndex();

 				// //Auto check line item based on field maintained
 				// this._oSmartTable.getTable().getPlugins()[0].addSelectionInterval(iSelectedRow, iSelectedRow, {
 				// 	ignore: true
 				// });
 			} else {
 				this.getModel().setProperty(sPath + "/AllocatedQty", null);
 			}
 			this.setAppBusy(false);
 		},
 		cancelCurrentChanges: function () {
 			if (this._anyOngoingChanges()) {
 				MessageBox.confirm(this.getResourceText("cancelChangesConfirm"), {
 					onClose: function (sAction) {
 							if (sAction === MessageBox.Action.OK) {
 								this.setDirtyState(false);
 								this._oModel.resetChanges();
 								this._oSmartTable.rebindTable(true);
 								return;
 							}
 						}
 						.bind(this)
 				});
 			}
 		},
 		handlePostStock: function (oEvent) {
 			var aSelectedContexts = this.extensionAPI.getSelectedContexts(),
 				oFloat;
 			this._iPostingLength = aSelectedContexts.length;
 			for (var t = 0; t < aSelectedContexts.length; t++) {
 				if (isNaN(parseFloat(aSelectedContexts[t].getObject().AllocatedQty))) {
 					// is not float    
 					oFloat = false;
 					break;
 				}
 			}

 			if (oFloat !== false) {
 				if (this._anyOngoingChanges()) {
 					MessageBox.confirm(this.getResourceText("confirmPostMsg"), {
 						onClose: function (sAction) {
 								if (sAction === MessageBox.Action.OK) {
 									this._oBusyDialog = new BusyDialog({
 										showCancelButton: false,
 										title: "Documents being created...",
 										text: this.getResourceText("Please wait..!")
 									});
 									this.extensionAPI.securedExecution(this._postStockAllocationMass.bind(this), {
 										busy: {
 											set: true,
 											check: true
 										},
 										dataloss: {
 											popup: false,
 											navigation: false
 										}
 									});
 									return;
 								}
 							}
 							.bind(this)
 					});
 				}
 			} else {
 				MessageToast.show(this.getResourceText("allocatedQtyError"));
 			}
 		},
 		_postStockAllocationMass: function (oEvent) {
 			var aSelectedContexts = this.extensionAPI.getSelectedContexts();
 			this._iPostingLength = aSelectedContexts.length;
 			this._oBusyDialog.open();
 			sap.ui.getCore().getMessageManager().removeAllMessages();
 			for (var t = 0; t < aSelectedContexts.length; t++) {
 				this._postStockAllocation(aSelectedContexts[t]);
 			}
 		},
 		_postStockAllocation: function (oSelectedContext) {
 			var oStockAllocation = oSelectedContext.getObject();
 			oStockAllocation.ReturnResponse = undefined;
 			oStockAllocation.AllocatedQty = oStockAllocation.AllocatedQty.toString();
 			oStockAllocation.UnrestrictedQty11r0 = oStockAllocation.UnrestrictedQty11r0.toString();
 			oStockAllocation.UnrestrictedQty1200 = oStockAllocation.UnrestrictedQty1200.toString();
 			oStockAllocation.BalanceQty11r0 = oStockAllocation.BalanceQty11r0.toString();
 			oStockAllocation.BalanceQty1200 = oStockAllocation.BalanceQty1200.toString();

 			this.getModel().create("/Stock_AllocationSet", oStockAllocation, {
 				groupId: "postStockAllocationsGroup",
 				changeSetId: "postStockAllocationsChangeSet",
 				success: function (data, response) {
 					this._iPostingLength--;
 					if (this._iPostingLength === 0) {
 						this._oModel.resetChanges();
 						this.extensionAPI.refreshTable();
 						this._oSmartTable.getTable().getPlugins()[0].clearSelection();
 						var msg = "Created Successfully.";
 						MessageBox.success(msg, {
 							title: "Success"
 						});
 						this._oBusyDialog.close();
 					}
 				}.bind(this),
 				error: function (err) {
 					// this._iPostingLength--;
 					// if (this._iPostingLength === 0) {
 					// 	this.showMessages();
 					// 	this._oBusyDialog.close();

 					// }
 					MessageBox.error(this.getResourceText("notCreated"), {
 						// title: "test"
 					});
 				}.bind(this)
 			});
 		},
 		_anyOngoingChanges: function () {
 			var bDirtyState = false;
 			if (this.getDirtyState()) {
 				bDirtyState = true;
 			} else {
 				MessageToast.show(this.getResourceText("noChangesMsg"));
 			}
 			return bDirtyState;
 		},
 		showMessages: function (oEvent) {
 			if (!this._oMessagePopover) {
 				this._oMessagePopover = new MessagePopover({
 					items: {
 						path: "message>/",
 						template: new MessageItem({
 							activeTitle: true,
 							description: "{message>description}",
 							type: "{message>type}",
 							title: "{message>message}"
 						})
 					}
 				});
 				this._oMessagePopover.setModel(sap.ui.getCore().getMessageManager().getMessageModel(), "message");
 			}
 			this._oMessagePopover.openBy(oEvent ? oEvent.getSource() : this.byId("showMessagesButton"));
 		}
 	};
 });