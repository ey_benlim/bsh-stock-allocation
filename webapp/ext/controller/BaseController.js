sap.ui.define(["sap/ui/core/BusyIndicator"
], function (BusyIndicator) {
	"use strict";
	return {
		/**
		 * Convenience method for accessing the router in every controller of the application
		 * @public
		 * @returns {sap.ui.core.routing.Router} the router of this component
		 */
		getRouter: function () {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},
		
		/**
		 * Convenience method for getting the view model by name in every controller of the application
		 * @public
		 * @param {string} sName the model name
		 * @returns {sap.ui.model.Model} the model instance
		 */
		getModel: function (sName) {
			return this.getView().getModel(sName);
		},
		
		/**
		 * Convenience method for setting the view model in every controller of the application
		 * @public
		 * @param {sap.ui.model.Model} oModel the model instance
		 * @param {string} sName the model name
		 * @returns {sap.ui.model.Model} the model instance
		 */
		setModel: function (oModel, sName) {
			return this.getView().setModel(oModel, sName);
		},
		
		/**
		 * Convenience method for getting the resource Bundle
		 * @public
		 * @returns {sap.ui.model.resource.ResourceModel} the resource model of the component
		 */
		getResourceBundle: function () {
			return this.getOwnerComponent().getModel("i18n").getResourceBundle();
		},
		
		/**
		 * Convenience method for getting the text from resource Bundle
		 * @public
		 * @param {string} sResource name of the resource
		 * @param {array} aParameters Array of dynamic contenr
		 * @returns {string} the i18n text
		 */
		getResourceText: function (sResource, aParameters) {
			return this.getResourceBundle().getText(sResource, aParameters);
		},
		
		/**
		 * Convenience method for returning Validator instance
		 * @public
		 * @returns {object} the instance of Validator
		 */
		getValidator: function () {
			return this.getOwnerComponent().getValidator();
		},
		
		/**
		 * Convenience method for returning Error Handler instance
		 * @public
		 * @returns {object} the instance of Error Handler instance
		 */
		getErrorHandler: function () {
			return this.getOwnerComponent().getErrorHandler();
		},
		
		/**
		 * Indicates error handler that external handler will handle errors and no action required
		 * @param {boolean} bExternalHandler Indicate whether error handler is generic or external
		 * @public
		 */
		setoDataErrorMessageHandler: function (bExternalHandler) {
			this.getErrorHandler().setoDataErrorMessageHandler(bExternalHandler);
		},
		
		/**
		 * Close error message popover
		 * @public
		 */
		closeErrorMessagePopover: function () {
			this.getErrorHandler().closeErrorMessagePopover();
		},
		
		/**
		 * Set the default source of message popover
		 * @param {sap.ui.core.Control} oSource Source of message popover launching
		 * @public
		 */
		setDefaultMessagePopoverLaunchSource: function (oSource) {
			this.getErrorHandler().setDefaultMessagePopoverLaunchSource(oSource);
		},
		
		/**
		 * Display log captured in the error handler
		 * @public
		 */
		displayLog: function () {
			this.getErrorHandler().displayLog();
		},
		
		/**
		 * Set message log
		 * @param {array} aMessages Array of messages
		 * @param {boolean} bAddMessages Adds messages to the existing log
		 * @public
		 */
		setMessageLog: function (aMessages, bAddMessages) {
			var aMessagesFinal = [];
			if (bAddMessages) {
				aMessagesFinal = this.getOwnerComponent().getModel("message").getProperty("/messages");
			}
			if (aMessages) {
				aMessagesFinal = aMessagesFinal.concat(aMessages);
			}
			this.getOwnerComponent().getModel("message").setProperty("/messages", aMessagesFinal);
		},
		
		/**
		 * Return the current user
		 * @public
		 * @returns {string} Return current user
		 */
		getCurrentUser: function () {
			var sCurrentUser = "";
			sCurrentUser = sap.ushell.Container.getService("UserInfo").getId();
			sCurrentUser = sCurrentUser === "DEFAULT_USER" ? "CLOUD_RFC" : sCurrentUser;
			return sCurrentUser;
		},
		
		/**
		 * Set the page dirty
		 * @param {boolean} bDirtyState Is Dirty?
		 * @public
		 */
		setDirtyState: function (bDirtyState) {
			if (sap.ushell.Container) {
				sap.ushell.Container.setDirtyFlag(bDirtyState);
			}
			if (!bDirtyState) {
				this._oModel.resetChanges();
			}
		},
		
		/**
		 * Get the page state status
		 * @public
		 * @returns {boolean} Return Is Dirty?
		 */
		getDirtyState: function () {
			return sap.ushell.Container.getDirtyFlag() || this.getView().getModel().hasPendingChanges(true);
		},
		
		/**
		 * Aggregates Messages
		 * @param {boolean} bAggregateMessage Aggregate error handler messages?
		 * @public
		 */
		setAggregateMessage: function (bAggregateMessage) {
			this.getOwnerComponent().getErrorHandler().setAggregateMessage(bAggregateMessage);
		},
		
		validateControlContent: function (e, t, r, n, s) {
			if (!e.getValue() || e.getValue() === "") {
				e.setValueState(sap.ui.core.ValueState.None);
				this.setAppBusy(false);
				return;
			}
			if (!n) {
				n = {};
			}
			if (!s) {
				var o = this._oModel;
			} else {
				o = s;
			}
			if (n.handleBusy) {
				this.setAppBusy(true);
			}
			this.setoDataErrorMessageHandler(true);
			o.read(t, jQuery.extend(false, n, {
				success: function (t) {
						if (typeof r === "function") {
							var s = t;
							if (t.results) {
								s = t.results.length > 0 ? t.results[0] : {};
							}
							r(s);
						}
						if (t.results && t.results.length === 0) {
							e.setValueState(sap.ui.core.ValueState.Error);
						} else {
							e.setValueState(sap.ui.core.ValueState.None);
						}
						if (n.handleBusy) {
							this.setAppBusy(false);
						}
					}
					.bind(this),
				error: function () {
						e.setValueState(sap.ui.core.ValueState.Error);
						e.setValueStateText("Invalid");
						if (n.handleBusy) {
							this.setAppBusy(false);
						}
					}
					.bind(this)
			}));
		},
		
		/**
		 * Convenient method to display busy indicator
		 * @param {boolean} bAppBusy Is App Busy?
		 * @public
		 */
		setAppBusy: function (bAppBusy) {
			if (bAppBusy) {
				BusyIndicator.show();
			} else {
				BusyIndicator.hide();
			}
		},
		getControlPropertyManager: function () {
			return this.getOwnerComponent().getControlPropertyManager();
		},
		getControlTooltipManager: function () {
			return this.getOwnerComponent().getControlTooltipManager();
		},
		getHideEmptySectionManager: function () {
			return this.getOwnerComponent().getHideEmptySectionManager();
		}
	};
});