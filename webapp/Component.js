sap.ui.define(["sap/suite/ui/generic/template/lib/AppComponent"], function (AppComponent) {
	return AppComponent.extend("ZSD_STOCK_ALLOCATION.Component", {
		metadata: {
			"manifest": "json"
		}
	});
});